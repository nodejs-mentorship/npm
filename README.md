# npm

## Task

1. create package.json file
2. By using npm add package named moment to your package.json file by using npm install moment -save
   You should also see that node_modules folder is created next to package.json file and that it contains moment package.
3. Find out what are dependencies in package.json and what ^ and ~ signs in front of package versions mean. Also, find out what will happen when we put "\*" instead of version number.
4. Create index.js file, require "moment" library in it and write code that will print current formatted time to console.
5. Run file using
   **"node index.js"**
   on command line.

6. Add development dependency named jasmine-node.
7. install globaly package named "nodemon" and investigate what is it used for. 8. use nodemon to start your index.js file
